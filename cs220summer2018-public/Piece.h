//Piece.h

#ifndef PIECE_H
#define PIECE_H

#include <string>

class Piece {

public:
  
  //Create a brand new piece
  Piece() : has_moved(false) {}

  //Indicate that the piece has now moved
  void set_has_moved() { has_moved = true; }

  //Report whether the piece has ever moved
  bool get_has_moved() const { return has_moved; }
  
  //Indicates whether the given start and end coordinates describe
  //a move that would be considered valid for this piece
  //This will be overridden in derived classes.
  bool legal_move_shape(const std::string& start,
			const std::string& end) const {
    std::string s = start; //only here to avoid unused parameter warning
    std::string e = end; //only here to avoid unused parameter warning
    return false;
  }

private:

  //indicates whether piece has moved from its starting location
  bool has_moved;  
};

#endif
