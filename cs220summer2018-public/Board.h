//Board.h
#ifndef BOARD_H
#define BOARD_H

#include <map>

class Board {

public:

   //Reports whether piece exists at the given location,
   //and if so, what color piece it is.  Return value of:
   //  <false, false> or <false, true> indicates no piece
   //  <true, false> indicates black piece
   //  <true, true> indicates white piece
   std::pair<bool, bool> color_at(std::string loc) const;

   //Given a path direction argument, indicates whether the length squares on the path
   //after the starting coordinate are clear of pieces.  Valid direction values for 
   //this assignment are "n", "s", "e", "w", indicating north(upward), south(downward),
   //east(rightward), and west(leftward), respectively.
   //Return value of:
   //   <false, false> or <false, true> indicates described direction is invalid
   //                                   or that path goes off of the board
   //   <true, false> indicates path is valid but that some piece exists on it
   //   <true, true>  indicates path is valid and that no piece is present on it
   std::pair<bool, bool> path_is_legal_and_clear(std::string start_loc,
						 std::string dir,
						 size_t length) const;

  
   //Attempts to add the given piece type to the board, at the given location,
   //using piece notation described below.  Returns true if successful, and
   //false if the given piece_type is not valid (i.e. not one of 'K','k','R','r'),
   //the specified location is not a valid board location, or if the specified
   //location is occupied.  
   bool add_piece(std::string loc, char piece_type);

   
   //Attempts to remove from the board the piece at the given location.
   //Returns true if successful, false if the specified location is not
   //a valid board location, or if no piece exists at that location.
   bool remove_piece(std::string loc);

   
   //Returns a string representation of the current board state
   std::string to_string() const;

private:

   //Denotes the pieces present on the board.  The char denotes piece type at the given
   //key coordinate, using upper-case for white pieces, and lower-case for black pieces.
   //Valid piece notation for this assignment is 'K' for white king, 'k' for black king,
   //'R' for white rook, and 'r' for black rook.
   std::map<std::string, char> occ;

};


#endif
