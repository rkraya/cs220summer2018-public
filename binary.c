#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  unsigned int values[100];
  for(int i = 0; i < 100; i++) values[i]=rand();
  FILE* fp=fopen("foo.txt","w");
  if(!fp) return 1;
  for(int i = 0; i < 100; i++) fprintf(fp,"%u\n",values[i]);
  fclose(fp);
}
