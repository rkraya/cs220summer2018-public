//collect_input.c
//601.220, Summer 2018
//Coding example from class to demonstrate scanf

#include <stdio.h>

int main(){

  //declare two int variables, and initialize with dummy values 5 and 7
  int x = 5;
  int y = 7;

  //Attempt to read in two integers, store them as x and y, respectively.
  //Return value from scanf is stored in num_collected, and indicates how
  //many values were successfully collected.
  //The return value may be EOF (a special int meaning end-of-file) if no
  //input is remaining.
  //RECALL: when running interactively, user will type Ctrl-D to
  //indicate end of input, and sometimes that needs to be typed twice
  printf("Enter two integers: ");
  int num_collected = scanf("%d%d", &x, &y);

  //check return value from scanf
  if (num_collected == 2){
    printf("x = %d, y = %d\n\n", x, y);
  } else if (num_collected == EOF) {
    printf("End of file detected before any ints.\n\n");
  } else {
    printf("Couldn't read in two integers.\n\n");
  }

  //Declare a char variable named c and initialize it to store '?'
  char c = '?';

  //Read in a character and output it.
  //Note the space before %c in scanf format string - it tells scanf to
  //ignore any leading whitespace characters (blanks, tabs, and newlines)
  //When reading in numeric types using %d or %f, we don't need to include 
  //a space there; leading whitespace is automatically ignored in that case
  printf("Enter a character: ");
  if (scanf (" %c", &c) == 1) { //note scanf is called inside if condition
    printf("c = %c\n", c);
  } else {
    printf("Didn't collect a character.\n\n");
  }
  return 0;
}
