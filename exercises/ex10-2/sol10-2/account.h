#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>
#include <iostream>

class Account {

 public:

 Account() : balance(0.0) { }

 Account(double initial) : balance(initial) { }

  // Add credit amount to account
  void credit(double amt) {
    balance += amt;
  }

  // Subtract debit amount from account
  void debit(double amt) {
    balance -= amt;
  }
  
  // Return type
  double get_balance() const {
    return balance;
  }
  
  // Return object type
  virtual std::string type() const {
    return "Account";
  }

  Account& operator+=(int amt) {
    credit(amt);
    return *this;
  }

  Account& operator-=(int amt) {
    debit(amt);
    return *this;
  }

 private:

  double balance;
};

std::ostream& operator<<(std::ostream& os, const Account& c);


class CheckingAccount : public Account {

 public:
 
 CheckingAccount(double initial, double atm) :
  Account(initial), total_fees(0.0), atm_fee(atm) { }

  // Debit and incur ATM fee
  void cash_withdrawal(double amt) {
    total_fees += atm_fee;
    debit(amt + atm_fee);
  }

  // Get total fees incurred so far
  double get_total_fees() const {
    return total_fees;
  }

  // Return object type
  virtual std::string type() const {
    return "CheckingAccount";
  }

 private:
  double total_fees;
  double atm_fee;
};

class SavingsAccount : public Account {

 public:
  
 SavingsAccount(double initial, double rate) :
  Account(initial), annual_rate(rate) { }
  
  // Not implemented here; usual compound interest calc
  double total_after_years(int years) const;
  
  // Return object type
  virtual std::string type() const {
    return "SavingsAccount";
  }
  
 private:

  double annual_rate;
};

#endif
