#include <iostream>
#include "account.h"

//
// Chapter 2: Overloading +=, -=
//

// TODO: This will not compile.  Using the `credit` and `debit`
//       member functions as a starting point, implement
//       operator+= and operator-= so that this compiles and
//       prints the correct series of balances for each account.

using std::cout;
using std::endl;

void test_account(Account& acct) {
  cout << acct << endl;
  acct += 100;
  cout << acct << endl;
  acct -= 200;
  cout << acct << endl;
}

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1500.0, 2.00);
  SavingsAccount saving(0.0, 0.05);

  test_account(acct);
  test_account(checking);
  test_account(saving);
  
  return 0;
}
