#include <cassert>

template< typename Data >
Data stack< Data >::pop( void )
{
  assert( _size>0 );
  _size--;
  return _data[_size];
}
template< typename Data >
void stack< Data >::push( Data d )
{
  assert( _reserved>0 );
  if( _reserved==_size )
  {
    _reserved *= 2;
    Data* _temp_data = new Data[ _reserved ];
    // [NOTE] We are using the assignment operator rather than memcpy to
    // support deep copies, in case the template class supports them.
    for( int i=0 ; i<_size ; i++ ) _temp_data[i] = _data[i];
    delete[] _data;
    _data = _temp_data;
  }
  _data[_size++] = d;
}
