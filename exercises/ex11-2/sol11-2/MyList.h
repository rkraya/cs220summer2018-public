#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>
#include "MyNode.h"

//A linked list hold int data
class MyList {

private:
  MyNode* head;  //pointer to first node in linked list

public:
  MyList():head(nullptr) {}  //create empty linked list

  // *** solution code ***
  // Destructor
  ~MyList() {
    MyNode* cur = head;
    while(cur != nullptr) {
      MyNode *next = cur->next;
      delete cur;
      cur = next;
    }
  }

  // Copy constructor
  MyList(const MyList& o) {
    head = nullptr;
    if(o.head != nullptr) {
      insertAtHead(o.head->data);
      MyNode *cur = o.head->next;
      while(cur != nullptr) {
        insertAtTail(cur->data);
        cur = cur->next;
      }
    }
  }
  
  MyList& operator=(const MyList& o) {
    MyNode* cur = head;
    while(cur != nullptr) {
      MyNode *next = cur->next;
      delete cur;
      cur = next;
    }
    if(o.head != nullptr) {
      insertAtHead(o.head->data);
      MyNode *cur = o.head->next;
      while(cur != nullptr) {
        insertAtTail(cur->data);
        cur = cur->next;
      }
    }    
    return *this;
  }

  // *** solution code ***

  void insertAtHead(int d); //create new MyNode and add it at head
  
  void insertAtTail(int d); //create new MyNode and add it at head

  //output the entire list, with data elements separated by spaces
  friend std::ostream& operator<<(std::ostream& os, const MyList& list);

};

#endif
