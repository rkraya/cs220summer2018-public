
#include <iostream>
#include <string>
#include "MyNode.h"
#include "MyList.h"

using std::cout;
using std::endl;

int main() {

  MyList list;

  list.insertAtHead(3);
  list.insertAtHead(2);
  list.insertAtHead(1);

  cout << "list = " << list << endl << endl;;
  
  MyList copy(list); //create a copy of first list
  
  cout << "list = " << list << endl;
  cout << "copy = " << copy << endl << endl;;
  
  cout << "now adding to copy at head" << endl;
  copy.insertAtHead(0);
  
  cout << "list = " << list << endl;
  cout << "copy = " << copy << endl << endl;

  cout << "now adding to copy at tail" << endl;
  copy.insertAtTail(4);
  
  cout << "list = " << list << endl;
  cout << "copy = " << copy << endl << endl;

}

