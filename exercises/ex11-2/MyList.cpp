#include "MyList.h"
#include "MyNode.h"


void MyList::insertAtHead(int d) {
  head = new MyNode(d, head);
}


void MyList::insertAtTail(int d){
  if (head == nullptr) {
    head = new MyNode(d, nullptr);

  } else {
    MyNode* cur = head;
    while (cur->next != nullptr) {
      cur = cur->next;
    }
    cur->next = new MyNode(d, nullptr);
  }
}

std::ostream& operator<<(std::ostream& os, const MyList& list) {

  //Run through all items in list, output them one by one                                                                                           
  for (MyNode* cur = list.head; cur != nullptr; cur = cur->next) {
     std::cout << cur->data << " ";
  }

  return os;
}
