#include <iostream>
#include "account_simple.h"

//
// Chapter 2: The const gardener
//

// This main function is similar to main1.cpp, but the
// objects are first passed (with type `const Account&`)
// to a function called `print_balances`, which does the
// printing.

// Use `make main2` to compile and `./main2` to run

// TODO: This won't compile at first; modify
//       account_simple.h so this compiles and runs.
//       You should not modify this file.

using std::cout;
using std::endl;

void print_balances(const Account& acct,
                    const Account& checking,
                    const Account& saving)
{
  cout << "Account: "         << acct.get_balance()     << endl;
  cout << "CheckingAccount: " << checking.get_balance() << endl;
  cout << "SavingsAccount: "  << saving.get_balance()   << endl;
}

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1000.0, 2.00);
  SavingsAccount saving(1000.0, 0.05);

  acct.debit(100.0);
  checking.cash_withdrawal(100.0);
  saving.debit(100.0);

  print_balances(acct, checking, saving);
  
  return 0;
}
