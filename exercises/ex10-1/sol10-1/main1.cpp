#include <iostream>
#include "account_simple.h"

//
// Chapter 1: The easy part
//

// This main function:
// - Instantiates Account, CheckingAccount and SavingsAccount
//   objects using a non-default constructor in all cases.
// - Calls some member functions to modify the balance
// - Prints out the final balance to check that they work

// Use `make main1` to compile and `./main1` to run

// There are no TODOs here besides to read the code

using std::cout;
using std::endl;

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1000.0, 2.00);
  SavingsAccount saving(1000.0, 0.05);

  acct.debit(100.0);
  checking.cash_withdrawal(100.0);
  saving.debit(100.0);

  cout << "Account: "         << acct.get_balance()     << endl
       << "CheckingAccount: " << checking.get_balance() << endl
       << "SavingsAccount: "  << saving.get_balance()   << endl;
  
  return 0;
}
