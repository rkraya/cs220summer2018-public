#include <iostream>
#include "account_virtual.h"

//
// Chapter 4: A tale of 2 sizeofs, part two
//

// This is just like main3.cpp except now we:
// #include "account_virtual.h"
//
// In that header, the type() member function in each of the
// three Account classes (Account, CheckingAccount &
// SavingsAccount) is a virtual function.  This is what allows
// the print_types function to print the correct type for each,
// even though they are passed in using the base class type.
//
// Resolve the TODOs in the comments below and then think about
// and answer these questions:
// - Are the sizes different in this example than they were in
//   main3.cpp?  If so, why?
// - What would the size of the objects be if there were 2
//   virtual member functions instead of 1?

using std::cout;
using std::endl;

void print_sizes_1(const Account& acct,
                   const CheckingAccount& checking,
                   const SavingsAccount& saving)
{
    // TODO use cout & sizeof to print the sizes of these objects
}

void print_sizes_2(const Account& acct,
                   const Account& checking,
                   const Account& saving)
{
    // TODO use cout & sizeof to print the sizes of these objects
}

void print_types(const Account& acct,
                 const Account& checking,
                 const Account& saving)
{
    // Confirming that the virtual functions work as expected
    cout << "acct.type() = "     << acct.type()     << endl
         << "checking.type() = " << checking.type() << endl
         << "saving.type() = "   << saving.type()   << endl;
}

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1000.0, 2.00);
  SavingsAccount saving(1000.0, 0.05);

  // TODO: call print_sizes_1 and print_sizes_2 with appropriate
  //       arguments to print the object sizes
  
  print_types(acct, checking, saving);
  
  return 0;
}
